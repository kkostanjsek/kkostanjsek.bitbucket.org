
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
var partyData1 = {
  firstNames: "Jan",
  lastNames: "Teleban",
  dateOfBirth: "1998-01-01",
  gender: "MALE"
};
var partyData2 = {
  firstNames: "Tine",
  lastNames: "Bine",
  dateOfBirth: "1999-02-02",
  gender: "MALE"
};
var partyData3 = {
  firstNames: "Marko",
  lastNames: "Žarko",
  dateOfBirth: "1997-03-03",
  gender: "MALE"
};

function vsiPodatki(){
  generirajPodatke(1);
  
}
 
function generirajPodatke(stPacienta) {
  if(stPacienta > 3) return;
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        console.log(data.ehrId + " " + stPacienta);
        var ehrId = data.ehrId; //uspešno pridobljen nopv ehr id
        var partyData;
        switch(stPacienta){
          case 1:
            partyData = partyData1;
            break;
          case 2:
            partyData = partyData2;
            break;
          case 3:
            partyData = partyData3;
            break;
        }
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              //uspešnu zapisani podatki za pacienta
              switch (stPacienta) {
                case 1:
                  dodajMeritev("2000-01-01T12:34:56", ehrId, 170, 80, function(){
                    dodajMeritev("2000-01-02T12:34:56", ehrId, 171, 81, function(){
                      dodajMeritev("2000-01-03T12:34:56", ehrId, 172, 82, function(){
                        dodajMeritev("2000-01-04T12:34:56", ehrId, 173, 83, function(){
                          document.getElementById("izbira-pacienta").innerHTML += "<option>"+ehrId+"</option>";
                          generirajPodatke(stPacienta+1);  
                        });  
                      });  
                    });  
                  });
                  break;
                  case 2:
                    dodajMeritev("2000-01-03T12:34:56", ehrId, 180, 76, function(){
                    dodajMeritev("2000-01-04T12:34:56", ehrId, 180, 76, function(){
                      dodajMeritev("2000-01-05T12:34:56", ehrId, 182, 75, function(){
                        dodajMeritev("2000-01-06T12:34:56", ehrId, 183, 76, function(){
                          document.getElementById("izbira-pacienta").innerHTML += "<option>"+ehrId+"</option>";
                          generirajPodatke(stPacienta+1);  
                        });  
                      });  
                    });  
                  });
                    break;
                    case 3:
                      dodajMeritev("2000-01-03T12:34:56", ehrId, 182, 80, function(){
                       dodajMeritev("2000-01-04T12:34:56", ehrId, 182, 80, function(){
                        dodajMeritev("2000-01-05T12:34:56", ehrId, 183, 81, function(){
                          dodajMeritev("2000-01-06T12:34:56", ehrId, 183, 81, function(){
                            document.getElementById("izbira-pacienta").innerHTML += "<option>"+ehrId+"</option>";
                            generirajPodatke(stPacienta+1);  
                         });  
                       });  
                      });  
                   });
                    break;
             }
              
            }
          },
          error: function(err) {
          	
          }
        });
      }
		});
}


function dodajMeritev(datum, ehr, visina, teza, callback){
  var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datum,
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza
		};
		var parametriZahteve = {
		    ehrId: ehr,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        callback(); 
      },
      error: function(err) {
      	
      }
		});
}

function preberiTeze(ehrid, callback){

  					$.ajax({
    				  url: baseUrl + "/view/" + ehrid + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			      callback(res);	
    			    },
    			    error: function() {
    			    	
    			    }
  					});
  			
}

function preberiVisine(ehrid, callback){

  					$.ajax({
    				  url: baseUrl + "/view/" + ehrid + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			      callback(res);	
    			    },
    			    error: function() {
    			    	
    			    }
  					});
  			
}
/*
function izpisiMeritve(){
  var ehr = document.getElementById("izbira-pacienta").value;
  console.log(ehr);
  preberiTeze(ehr, function(teze){
    
    document.getElementById("msg").innerHTML += "weight";
  })
  preberiVisine(ehr, function(visine){
    
    document.getElementById("msg").innerHTML += "height";
  })
}
*/
function naloziMapo(){

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [46, 14],
    zoom: 12
  };
  
  var poligoni = [];

  // Ustvarimo objekt mapa
  var mapa = new L.map('map', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  mapa.on('click', function(e){
    
    var latlng = e.latlng;
    var latm = latlng.lat;
    var lngm = latlng.lng;
    for (var i = 0; i < poligoni.length; i++) {
      var latlngpoligon = poligoni[i]._latlngs[0][0];
      var latp = latlngpoligon.lat;
      var lngp = latlngpoligon.lng;
      var razdalja = distance(latm, lngm, latp, lngp, 'K');
      
      if (razdalja < 0.75) {
        poligoni[i].setStyle({color: "GREEN"}); 
      }
      else {
        poligoni[i].setStyle({color: "BLUE"}); 
      }
    }
  });
  
  //https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json
  
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        
        var features = json.features;
        
        for (var i = 0; i < features.length; i++) {
          var feature = features[i];
          if (feature.geometry.type == "Polygon"){
            var koordinate = feature.geometry.coordinates;
            for (var j = 0; j < koordinate.length; j++) {
              var krog = koordinate[j];
              for (var k = 0; k < krog.length; k++) {
                var koord = krog[k];
                var x = koord[0];
                var y = koord[1];
                
                koord[0] = y;
                koord[1] = x;
                krog[k] = koord;
              }
              koordinate[j] = krog;
            }
            
            var poligon = L.polygon(koordinate, {color: "blue"});
            
            poligon.addTo(mapa);
            
            var properties = feature.properties;
            var ime = properties.name;
            var postna = properties["addr:postcode"];
            var ulica = properties["addr:street"];
            var hisnaSt = properties["addr:housenumber"];
            
            poligon.bindPopup(ime+ "<br>" + ulica + " " + hisnaSt);
            
            poligoni.push(poligon);
            
          }
          
        }
    }
  };
  xobj.send(null);
  
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
